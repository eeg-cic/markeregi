import csv
import os

hz = 1000
f = 1/hz

filename = input("Enter raw data file name (including .mrk): ")
if filename[0] == "\"":
    filename = filename[1:-1]

filenamecsv = filename[0:-4]+".csv"

try:
    os.rename(filename, filenamecsv)
except:
    pass

epochstr = input("Enter the duration of epoch (in seconds): ")
epoch = int(epochstr)
print ("Inserting marker...")


ifile = open(filenamecsv, "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output.mrk', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

et = 0
rownum = 0
scribble = [""]*3
tray = [""]*3

for row in reader:
    scribble = tray
    tray = row
    #Process the rest
    if rownum > 1:
        writer.writerow(scribble)
        #Check marker if emotion or interlude
        if scribble[2][0] == 'f' or scribble[2][0] == 'i' or scribble[2][0] == 'n':
            gap = int(tray[0]) - int(scribble[0])
            mark = int(gap/(hz*epoch))
            # print(scribble)
            # print(tray)
            # print(str(mark) + '  ' + str(gap))
            for i in range (mark):
                next = int(scribble[0])+((hz*epoch)*(i+1))
                placeholder = scribble[2][0:4] + "M"
                writer.writerow([str(next),str(next+1),placeholder])


    #Skip header
    if rownum == 0:
        writer.writerow(row)

    rownum += 1

    
writer.writerow(tray)
ifile.close()
ofile.close()
os.rename(filenamecsv, filename)
print ("Finished as Output.mrk")
